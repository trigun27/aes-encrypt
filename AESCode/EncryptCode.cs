﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace AESCode
{
    public class AES
    {
        private static readonly int _nb = 4;         //number of column of State
        private static readonly int _nk = 4;         //the key length (in 32-bit words)
        private static readonly int _nr = 10;        //number of rounds ib ciper cycle


        public static void Enrypt(Stream input, Stream output, string Key)
        {
            var state = HelpFunc.CreateArrBytes(_nb, _nb);

            int item;
            int read = 16, count = 16;
            var buffer = new byte[16];



            while ((read = input.Read(buffer, 0, count)) >= 0 )
            {
                if (read!=0)
                {
                    for (int i = 0; i < _nb; i++)
                    {
                        for (int j = 0; j < _nb; j++)
                        {
                            if (buffer.Length < 16)
                                state[i][j] = 0;
                            else
                                state[i][j] = buffer[i + 4*j];
                        }
                    }

                    var keyShedule = KeyExpansion(Key);

                    AddRoundKey(state, keyShedule, 0);

                    for (int rnd = 1; rnd < _nr; rnd++)
                    {
                        SubBytes(state, false);
                        ShiftRows(state, false);
                        MixColumns(state, false);
                        AddRoundKey(state, keyShedule, rnd);
                    }

                    SubBytes(state, false);
                    ShiftRows(state, false);
                    AddRoundKey(state, keyShedule, _nr);

                    for (int j = 0; j < _nk; j++)
                    {
                        for (int i = 0; i < _nk; i++)
                        {
                            output.WriteByte(state[i][j]);
                        }
                    }
                }
                else
                {
                    break;
                }
               
            }
            output.Flush();
            output.Seek(0, SeekOrigin.Begin);
        }

        public static void Decrypte(Stream input, Stream output, string Key)
        {
            var state = HelpFunc.CreateArrBytes(_nb, _nb); // посмотреть правильно ли заполняется State


            int count = 16;
            var buffer = new byte[16];

            while ((input.Read(buffer, 0, count)) > 0)
            {

                for (int i = 0; i < _nb; i++)
                {
                    for (int j = 0; j < _nb; j++)
                    {
                        state[i][j] = buffer[i + 4 * j];

                    }
                }

                var keyShedule = KeyExpansion(Key);

                state = AddRoundKey(state, keyShedule, _nr);

                for (int i = _nr-1; i >= 1; i--)
                {
                    ShiftRows(state, true);
                    SubBytes(state, true);
                    AddRoundKey(state, keyShedule, i);
                    MixColumns(state, true);       
                }


                ShiftRows(state, true);
                SubBytes(state, true);               
                AddRoundKey(state, keyShedule, 0);


                for (int j = 0; j < _nk; j++)
                {
                    for (int i = 0; i < _nk; i++)
                    {
                        output.WriteByte(state[i][j]);
                    }
                }

            }
            output.Flush();
            output.Seek(0, SeekOrigin.Begin);
        }


        //меняет байты на табличные значения
        private static void SubBytes(byte[][] state, bool inv)
        {
            var box = (byte[]) (!inv ? Boxes.Sbox : Boxes.InvBox);

            for (int i = 0; i < _nb; i++)
            {
                for (int j = 0; j < _nb; j++)
                {
                    byte row = (byte) (state[i][j] / 0x10);
                    byte col = (byte) (state[i][j] % 0x10);
                                       
                    state[i][j] = box[16*row + col];
                }             

            }
        }

        //циклический сдвиг в матрице
        private static void ShiftRows(byte[][] state, bool inv)
        {
                
            int count = 1;
            state[0] = state[0];

            if (!inv)
            {
                
                for (int i = 1; i < _nb; i++)
                {
                    state[i] = HelpFunc.LeftShift(state[i], count);
                    count++;
                }
            }
            else
            {
                for (int i = 1; i < _nb; i++)
                {
                    state[i] = HelpFunc.RightShift(state[i], count);
                    count++;
                }
            }
        }


        private static byte[][] MixColumns(byte[][] state, bool inv)
        {
            byte s0, s1, s2, s3;


            for (int i = 0; i < _nb; i++)
            {
                if (!inv)
                {
                     s0 = (byte) (HelpFunc.MultBy02(state[0][i]) ^ HelpFunc.MultBy03(state[1][i]) ^ state[2][i] ^ state[3][i]);
                     s1 = (byte) (state[0][i] ^ HelpFunc.MultBy02(state[1][i]) ^ HelpFunc.MultBy03(state[2][i]) ^ state[3][i]);
                     s2 = (byte) (state[0][i] ^ state[1][i] ^ HelpFunc.MultBy02(state[2][i]) ^ HelpFunc.MultBy03(state[3][i]));
                     s3 = (byte) ( HelpFunc.MultBy03(state[0][i]) ^ state[1][i] ^ state[2][i] ^ HelpFunc.MultBy02(state[3][i]));
                }
                else
                {
                     s0 = (byte) (HelpFunc.MultBy0E(state[0][i]) ^ HelpFunc.MultBy0B(state[1][i]) ^ HelpFunc.MultBy0D(state[2][i]) ^ HelpFunc.MultBy09(state[3][i]));
                     s1 = (byte) (HelpFunc.MultBy09(state[0][i]) ^ HelpFunc.MultBy0E(state[1][i]) ^ HelpFunc.MultBy0B(state[2][i]) ^ HelpFunc.MultBy0D(state[3][i]));
                     s2 = (byte) (HelpFunc.MultBy0D(state[0][i]) ^ HelpFunc.MultBy09(state[1][i]) ^ HelpFunc.MultBy0E(state[2][i]) ^ HelpFunc.MultBy0B(state[3][i]));
                     s3 = (byte) (HelpFunc.MultBy0B(state[0][i]) ^ HelpFunc.MultBy0D(state[1][i]) ^ HelpFunc.MultBy09(state[2][i]) ^ HelpFunc.MultBy0E(state[3][i]));
                }

                state[0][i] = s0;
                state[1][i] = s1;
                state[2][i] = s2;
                state[3][i] = s3;
            }
            return state;
        }


        private static byte[][] KeyExpansion(string key)
        {
            var keyShedule = HelpFunc.CreateArrBytes(_nb, _nb*(_nr + 1));

            var keySymbols = new List<byte>(Encoding.ASCII.GetBytes(key));

            //var keySymbols = GetBytes(key);

            if (keySymbols.Count < _nk*4 -1)
            {
                for (int i=keySymbols.Count+1; i<=_nk*4; i++ )
                    keySymbols.Add(0x01);
            }

            for (int i = 0; i < _nk; i++)
            { 
                for (int j = 0; j < _nk; j++)
                {
                    keyShedule[i][j] = keySymbols[i + _nk*j];
                }              
            }

            var buffMas = new byte[_nk];
            var s = new byte[_nk];

            for (int j = _nk; j < _nb * (_nr + 1); j++)
            {
                if (j % _nk == 0)
                {
                    buffMas = CopyBytes(keyShedule, 1, j - 1);

                    for (int k = 0; k < _nk; k++)
                    {
                        var sbrow = buffMas[k] / 0x10;
                        var sbcol = buffMas[k] % 0x10;

                        buffMas[k] = Boxes.Sbox[16 * sbrow + sbcol];
                    }

                    for (int k = 0; k < _nb; k++)
                    {
                        s[k] = (byte)(keyShedule[k][j - 4] ^ buffMas[k] ^ Boxes.Rcon[10 * k + j / _nk - 1]);
                    }

                    for (int k = 0; k < _nk; k++)
                    {
                        keyShedule[k][j] = s[k];
                    }
                }
                else
                {
                    for (int k = 0; k < _nb; k++)
                    {
                        s[k] = (byte)(keyShedule[k][j - 4] ^ keyShedule[k][j - 1]);
                    }

                    for (int k = 0; k < _nk; k++)
                    {
                        keyShedule[k][j] = s[k];
                    }
                }
            }
     


            return keyShedule;
        }

        private static byte[][] AddRoundKey(byte[][] state, byte[][] KeySheldule, int Round)
        {
           

            byte s0, s1, s2, s3;


            StringBuilder sb = new StringBuilder();
            sb.Append('[');
            for (int i = 0; i < state.Length; i++)
            {
                sb.Append('[');
                for (int j = 0; j < state[i].Length; j++)
                {
                    sb.Append(state[i][j].ToString() + ',');
                }
                sb.Append("],\r\n");
            }
            sb.Append(']');

            using (StreamWriter outfile = new StreamWriter(@"D:\OneDrive\Reps\AESmachine\AESmachine\bin\Debug\test.txt", true))
            {
                outfile.Write(sb.ToString());
            }



            for (int i = 0; i < _nk; i++)
            {
                s0 = (byte) (state[0][i] ^ KeySheldule[0][_nb*Round + i]);
                s1 = (byte) (state[1][i] ^ KeySheldule[1][_nb*Round + i]);
                s2 = (byte) (state[2][i] ^ KeySheldule[2][_nb*Round + i]);
                s3 = (byte) (state[3][i] ^ KeySheldule[3][_nb*Round + i]);

                state[0][i] = s0;
                state[1][i] = s1;
                state[2][i] = s2;
                state[3][i] = s3;
            }

            return state;
        }



        public static byte[] CopyBytes(byte[][] arr, int i, int j)
        {
            var mas = new byte[4];

            for (int k = 0; k < _nk - 1; k++)
            {
                mas[k] = arr[i][j];
                i++;
            }
            mas[3] = arr[0][j];
          
            return mas;
        }

    }
}
