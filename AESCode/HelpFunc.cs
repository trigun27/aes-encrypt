﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AESCode
{
    class HelpFunc
    {
        public static byte[][] CreateArrBytes(int count1, int count2)
        {
            var arr = new byte[count1][];
            for (int i=0;i<count1;i++)
                arr[i] = new byte[count2];

            return arr;
        }


        public static byte[] LeftShift(byte[] Row, int count)
        {
            var row = new byte[4]; 
            row = Row;
           
            /*var rowl = new List<byte>(Row);*/
            for (int k = 0; k < count; k++)
            {
                var buff = Row[0];
                for (int i = 0; i < Row.Length-1; i++)
                {
                    row[i] = Row[i + 1];
                    if (i == Row.Length - 2)
                        row[i+1] = buff;
                }
            }
         

            return row;
        }

        public static byte[] RightShift(byte[] Row , int count)
        {
            var row = Row;

            for (int k = 0; k < count; k++)
            {
                var buff = Row[Row.Length-1];
                for (int i = Row.Length - 1; i >= 0; i--)
                {
                    if (i == 0)
                        row[i] = buff;
                    else
                        row[i] = Row[i - 1];
                   
                }
            }

            return row;
        }

        public static byte MultBy02(byte num)
        {
            byte result;

            if (num < 0x80)
                result = (byte) (num << 1);
            else
                result = (byte) ((num << 1) ^ 0x1b);

            result = (byte) (result % 0x100);

            return result;
        }

        public static byte MultBy03(byte num)
        {
            return (byte) (MultBy02(num) ^ num);
        }

        public static byte MultBy09(byte num)
        {
            //return MultBy03(MultBy03(num));
            return (byte) (MultBy02(MultBy02(MultBy02(num))) ^ num); 
        }

        public static byte MultBy0B(byte num)
        {
            //return (byte) (MultBy09(num) ^ num);
            return (byte) (MultBy02(MultBy02(MultBy02(num)))^MultBy02(num)^num);
        }

        public static byte MultBy0D(byte num)
        {
            //return (byte) (MultBy0B(num) ^ num ^ num);
            return (byte) (MultBy02(MultBy02(MultBy02(num))) ^ MultBy02(MultBy02(num)) ^ num);
        }

        public static byte MultBy0E(byte num)
        {
            //return (byte) (MultBy0D(num) ^ num);
            return (byte)(MultBy02(MultBy02(MultBy02(num))) ^ MultBy02(MultBy02(num)) ^ MultBy02(num));
        }

    }
}
